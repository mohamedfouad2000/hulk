import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hulk/core/utils/components.dart';
import 'package:hulk/core/utils/styles.dart';
import 'package:hulk/features/home/data/Model/product_model/datum.dart';
import 'package:hulk/features/home/presentation/views/screens/product_details_screen.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

Widget productItem({
  required Datum model,
  required context,
}) {
  return GestureDetector(
    onTap: () {
      NavegatorPush(
          context,
          ProductDetails(
            model: model,
          ));
    },
    child: Padding(
      padding: const EdgeInsets.all(16),
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.bottomCenter,
        children: [
          CachedNetworkImage(
            height: 150,
            // fit: BoxFit.fitHeight,
            imageUrl: model.imagePath.toString(),
            placeholder: (context, url) => LoadingAnimationWidget.newtonCradle(
              size: 50,
              color: Colors.grey,
            ),
            errorWidget: (context, url, error) =>
                const Icon(Icons.error_outline_sharp),
          ),
          Positioned(
            bottom: -20,
            child: Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Text(
                model.name.toString(),
                style: StylesData.font16.copyWith(
                  color: colorHex(
                      model.datumClass?.color.toString() ?? '0xff000000'),
                ),
              ),
            ),
          )
        ],
      ),
    ),
  );
}
