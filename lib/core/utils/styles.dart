import 'package:flutter/material.dart';

abstract class StylesData {
  static TextStyle font24 = const TextStyle(
    color: Color(0xFF585858),
    fontSize: 24,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w500,
  );

  static TextStyle font14 = const TextStyle(
    color: Color(0xFF78847D),
    fontSize: 14,
    fontFamily: 'Inter',
    fontWeight: FontWeight.w500,
    height: 0.11,
  );

  static TextStyle fontPoppins14 = const TextStyle(
    color: Color(0xFF717171),
    fontSize: 14,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w400,
    height: 0,
  );

  static TextStyle font16 = const TextStyle(
    color: Colors.white,
    fontSize: 16,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w500,
    height: 0,
  );

  static TextStyle font18 = const TextStyle(
    color: Colors.white,
    fontSize: 18,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
    height: 0,
  );
  static TextStyle font20 = const TextStyle(
    color: Colors.black,
    fontSize: 20,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w500,
  );
  static TextStyle font12 = const TextStyle(
    color: Color(0xFFC1C1C1),
    fontSize: 12,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w400,
    height: 0,
  );

  static TextStyle font25 = const TextStyle(
    color: Colors.black,
    fontSize: 25.41,
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.w500,
    height: 0,
    letterSpacing: 0.53,
  );

  static TextStyle font19 = const TextStyle(
    color: Color(0xFFCC1100),
    fontSize: 19.06,
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.w500,
    height: 0,
    letterSpacing: 0.53,
  );
}
