class AssetsData {
  static const header = 'assets/images/header.png';
  static const footer = 'assets/images/footer.png';
  static const tqniaLogo = 'assets/images/tqnia.png';
  static const hullk = 'assets/images/halk.png';
  static const onboarding1 = 'assets/images/b11.jpg';
  static const onboarding2 = 'assets/images/b2.png';
  static const onboarding3 = 'assets/images/b3.png';
  static const product = 'assets/images/p.png';
  static const backGround = 'assets/images/bac.jpg';
}
